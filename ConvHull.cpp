#include <SFML/Graphics.hpp>
#include <bits/stdc++.h>
#include "ConvHull.h"
#include "polar.h"
#include "kirk.h"
using namespace std;

void pt::draw(sf::RenderWindow &window) {
    float rad = 5;
    sf::CircleShape ball(rad);
    ball.setFillColor(this->color);
    ball.setPosition(x, y);
    window.draw(ball);
}
void SceneStates::draw(sf::RenderWindow &window){
    for(auto p:points){
        p.draw(window);
    }
    for(auto line:lines){
        window.draw(line);
    }
}

bool rightAngl(pt a, pt b, pt c) {
    return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) < 0;
}

bool leftAngl(pt a, pt b, pt c) {
    return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) > 0;
}

double side(double ax,double bx,double ay,double by){
    return sqrt((bx-ax)*(bx-ax)+(by-ay)*(by-ay));
}

double S (pt d , pt e, pt f) {
    double a = side(e.x, d.x, e.y, d.y);
    double b = side(f.x, d.x, f.y, d.y);
    double c = side(e.x, f.x, e.y, f.y);
    double p = (a + b + c) / 2;
    double s = sqrt(p * (p - a) * (p - b) * (p - c));
    return s;
}

void ConvexHull::dotfill() {
        if (flag1 == 0) {
            srand(time(0));
            for (int i = 0; i < 50; i++) {
                vect.push_back({rand() % (int)((float)width/10*9) + 50, rand() % (int)((float)height/10*9) + 50});
            }
        }
        flag1 = 1;
    }

    void ConvexHull::Draw(sf::RenderWindow &window,int Meth) {
        vector<vector<pt>> Res;
        list<SceneStates> frames;
        if(vect.size()==0){
            dotfill();
        }
        vectLeft.clear();
        for (auto el : vect) {
            float rad = 5;
            sf::CircleShape ball(rad);
            ball.setFillColor(sf::Color::Green);
            ball.setPosition(el.x, el.y);
            window.draw(ball);
        }
        if(Meth==1){
            vectLeft=polar(vect,frames);
        }
        else if(Meth==2){
            vectLeft=kirk(vect,frames);
        }
            for (int i = 0; i < vectLeft.size(); i++) {
                float rad = 5;
                sf::CircleShape ball(rad);
                ball.setFillColor(sf::Color::Red);
                ball.setPosition(vectLeft[i].x, vectLeft[i].y);
                if (i > 0) {
                    sf::Vertex line[] =
                            {
                                    sf::Vertex(sf::Vector2f(vectLeft[i - 1].x + rad, vectLeft[i - 1].y + rad)),
                                    sf::Vertex(sf::Vector2f(vectLeft[i].x + rad, vectLeft[i].y + rad))
                            };

                    window.draw(line, 2, sf::Lines);
                }
                window.draw(ball);

            }
    }
