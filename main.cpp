#include <SFML/Graphics.hpp>
#include <bits/stdc++.h>
#include "ConvHull.h"
#include "polar.h"
#include "kirk.h"
/*
100 100
300 300
300 100
125 125
100 300
250 250
120 275
 */
using namespace std;
sf::RenderWindow window(sf::VideoMode(width,height), "Convex Hull");

class Menu{
private:
    sf::Texture texture;
    sf::Texture texture1;
    sf::Sprite sprite;
    sf::Sprite sprite1;
public:
    float x,y;
    Menu(float x, float y):x(x),y(y){
        texture.loadFromFile("C:\\Users\\yanak\\CLionProjects\\lab3\\images\\1.png");
        sprite.setTexture(texture);
        sprite.setScale(1,0.75);
        sprite.setPosition(x,y);
        texture1.loadFromFile("C:\\Users\\yanak\\CLionProjects\\lab3\\images\\2.png");
        sprite1.setTexture(texture1);
        sprite1.setScale(1,0.75);
        sprite1.setPosition(x,y+sprite.getGlobalBounds().height+10);
    }
    int testClick(float mouseX,float mouseY){
        if(sprite.getGlobalBounds().contains(mouseX,mouseY)){
            return 1;
        }
        else if(sprite1.getGlobalBounds().contains(mouseX,mouseY)){
            return 2;
        }
        else{
            return 0;
        }
    }
    void draw(sf::RenderWindow &window){
        window.draw(sprite);
        window.draw(sprite1);
    }
};

enum States {ShowMenu,Anim,Res};

int main() {
    ConvexHull a;
    list<SceneStates> frames;
    SceneStates scene;
    ifstream file("C:\\Users\\yanak\\CLionProjects\\lab3\\Dots.txt");
    int c, b;
    while(file >> c >> b)
    {
        a.vect.push_back({c,b});
    }
    if(a.vect.size()==0){
        a.dotfill();
    }
    States currentState=States::ShowMenu;
    Menu menu(width*4/10,height/10);
    clock_t prevTime=clock();
    int res;

    int cn=0;
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed){
                if(event.key.code==sf::Keyboard::Escape){
                    currentState=States::ShowMenu;
                }
            }
            if(event.type==sf::Event::MouseButtonPressed){
                if(currentState==States::ShowMenu){
                    res= menu.testClick(event.mouseButton.x,event.mouseButton.y);
                    if(res==1){
                        prevTime=clock();
                        frames.clear();
                        polar(a.vect,frames);
                        currentState=States::Anim;
                    }
                    else if (res==2){
                        prevTime=clock();
                        frames.clear();
                        kirk(a.vect,frames);
                        currentState=States::Anim;
                    }
                }
            }
        }
        window.clear();
        switch (currentState) {
            case States::ShowMenu:
                menu.draw(window);

                break;
            case States::Res:
                a.Draw(window,res);
                ++cn;
                break;
            case States::Anim:
                if(!frames.empty())
                {
                    window.clear();
                    //cout<<"a";
                    scene.draw(window);
                    if(clock()-prevTime> 100.0){
                        scene=frames.front();
                        frames.pop_front();
                        scene.draw(window);
                        prevTime=clock();
                    }
                }
                else{
                        currentState=States::Res;
                }
                break;
        }
        window.display();
    }
    return 0;
}