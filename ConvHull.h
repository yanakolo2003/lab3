
#include <bits/stdc++.h>
using namespace std;
#ifndef MAIN_CPP_CONVHULL_H
#define MAIN_CPP_CONVHULL_H

static const int width = 1500;
static const int height = 800;

class pt {
public:
    int x, y;
    sf::Color color;
    void draw(sf::RenderWindow &window);
};

class SceneStates{
public:
    std::vector<pt> points;
    std::vector<sf::VertexArray> lines;
    void draw(sf::RenderWindow &window);
};


bool cmp(pt a, pt b);
bool rightAngl(pt a, pt b, pt c);
bool leftAngl(pt a, pt b, pt c);
double side(double ax,double bx,double ay,double by);
double S (pt d , pt e, pt f);
class ConvexHull {
public:
    vector<pt> vect;
    vector<pt> vectLeft;
    int mas[height][2];
    int flag = 0;
    int flag1 = 0;
    void dotfill();
    void Draw(sf::RenderWindow &window,int Meth);
};
#endif //MAIN_CPP_CONVHULL_H
