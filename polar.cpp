
#include <SFML/Graphics.hpp>
#include <bits/stdc++.h>
#include "polar.h"
#include "ConvHull.h"
using namespace std;

vector<pt> polar(vector<pt> vect,list<SceneStates> &frames) {
    vector<pt> vect1=vect;
    SceneStates scene = SceneStates{vect1};
    frames.push_back(scene);
    vector<pt>vectLeft;
    vector<vector<pt>> Res;
    vector<pair<float, int>> angles;
    int sumX = 0;
    int sumY = 0;
    int index=0;
    for (int i = 0; i < vect.size(); i++) {
        sumX += vect[i].x;
        sumY += vect[i].y;
    }
    float centrX = (float) sumX / vect.size();
    float centrY = (float) sumY / vect.size();
    for (int i = 0; i < vect.size(); i++) {
        angles.push_back(make_pair(atan2(vect[i].y - centrY, vect[i].x - centrX), i));
    }
    sort(angles.begin(), angles.end());
    for (int i = 0; i < angles.size(); i++) {
        if(vect[angles[i].second].x<vect[angles[index].second].x){
            index=i;
        }
    }
    scene.points[index].color=sf::Color::Green;
    frames.push_back(scene);
    vectLeft.push_back(vect[angles[index].second]);
    reverse(angles.begin(), angles.end());
    vector <pair<float,int>> VEC;
    for (int i=0; i<index; i++){
        VEC.push_back(angles.back());
        angles.pop_back();
    }
    reverse(angles.begin(), angles.end());
    for(auto i:VEC){
        angles.push_back(i);
    }
    angles.push_back(angles.front());
    vectLeft.push_back(vect[index]);
    int Ind;
    for(int i=0;i<vect.size();i++){
        if((vect[i].x==vectLeft[i].x)&&(vect[i].y==vectLeft[i].y)){
            Ind=i;
        }
    }
    scene.points[Ind].color=sf::Color::Yellow;
    frames.push_back(scene);
    for (int i = 0; i < angles.size(); i++) {
        scene.points[angles[i].second].color=sf::Color::Green;
        frames.push_back(scene);
        while (vectLeft.size() > 1 &&
               !leftAngl(vectLeft[vectLeft.size() - 2], vectLeft[vectLeft.size() - 1], vect[angles[i].second])) {
            for (auto j=0;j <vect.size(); j++){
                if (vect[j].x==vectLeft.back().x && vect[j].y==vectLeft.back().y){
                    scene.points[j].color=sf::Color::Red;
                    frames.push_back(scene);
                }
            }
            vectLeft.pop_back();
        }
        vectLeft.push_back(vect[angles[i].second]);
        scene.points[angles[i].second].color=sf::Color::Yellow;
        frames.push_back(scene);

    }
    //Res.push_back();
    return vectLeft;//Res;
}
