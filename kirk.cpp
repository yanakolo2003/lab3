
#include <SFML/Graphics.hpp>
#include <bits/stdc++.h>
#include "kirk.h"
#include "ConvHull.h"
using namespace std;

vector<pt> kirk(vector<pt> vect, list<SceneStates> &frames) {
    vector<pt> vect1=vect;
    SceneStates scene = SceneStates{vect1};
    frames.push_back(scene);
    vector<pt> vectLeft;
    vector<pt> vectRight;
    int mas[800][2];
    for (int i = 0; i < 800; i++) {
        mas[i][0] = -1;
        mas[i][1] = -1;
    }

    for (int i = 0; i < vect.size(); i++) {
        if (mas[vect[i].y][0] != -1) {
            if (vect[mas[vect[i].y][0]].x > vect[i].x) {
                mas[vect[i].y][0] = i;
            }
            if (vect[mas[vect[i].y][1]].x < vect[i].x) {
                mas[vect[i].y][1] = i;
            }
        } else {
            mas[vect[i].y][0] = i;
            mas[vect[i].y][1] = i;
        }
    }
    vector<int> numer;
    set <pair<int,int>> SETleft,SETright;
    for (int i = 0; i < 800; i++) {
        if (mas[i][0] != -1) {
            scene = frames.back();
            numer.push_back(i);
            scene.points[mas[i][0]].color = sf::Color::Green;
            scene.points[mas[i][1]].color = sf::Color::Green;
            frames.push_back(scene);
            while (vectLeft.size() > 1 &&
                   leftAngl(vectLeft[vectLeft.size() - 2], vectLeft[vectLeft.size() - 1], vect[mas[i][0]])) {

                for(int q=0;q<800;q++){
                    if((vectLeft.back().x==vect[mas[q][0]].x)&&(vectLeft.back().y==vect[mas[q][0]].y)){
                        if (SETright.count({vect[mas[q][0]].x,vect[mas[q][0]].y})==0)
                            scene.points[mas[q][0]].color = sf::Color::Red;
                        break;
                    }
                }
                SETleft.erase({vectLeft.back().x,vectLeft.back().y});
                vectLeft.pop_back();
                frames.push_back(scene);
            }
            scene = frames.back();
            vectLeft.push_back(vect[mas[i][0]]);
            SETleft.insert({vect[mas[i][0]].x,vect[mas[i][0]].y});
            scene = frames.back();
            scene.points[mas[i][0]].color = sf::Color::Yellow;
            frames.push_back(scene);
            while (vectRight.size() > 1 &&
                   rightAngl(vectRight[vectRight.size() - 2], vectRight[vectRight.size() - 1], vect[mas[i][1]])) {

                for(int q=0;q<800;q++){
                    if((vectRight.back().x==vect[mas[q][1]].x)&&(vectRight.back().y==vect[mas[q][1]].y)){
                        if (SETleft.count({vect[mas[q][1]].x,vect[mas[q][1]].y})==0)
                            scene.points[mas[q][1]].color = sf::Color::Red;
                        break;
                    }
                }
                SETright.erase({vectRight.back().x,vectRight.back().y});
                vectRight.pop_back();
                frames.push_back(scene);
            }
            scene = frames.back();
            vectRight.push_back(vect[mas[i][1]]);
            SETright.insert({vect[mas[i][1]].x,vect[mas[i][1]].y});
            scene = frames.back();
            scene.points[mas[i][1]].color = sf::Color::Yellow;
            frames.push_back(scene);
        }
    }
    reverse(vectRight.begin(), vectRight.end());
    vectRight.push_back(vectLeft[0]);
    vectLeft.insert(vectLeft.end(), vectRight.begin(), vectRight.end());
    return vectLeft;
}